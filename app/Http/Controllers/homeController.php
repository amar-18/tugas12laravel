<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class homeController extends Controller
{
    public function index(){
        return view('form');
    }
    public function login(Request $request){
        $fname = strtoupper($request->firstname);
        $lname = strtoupper($request->lastname);

        return view('welcome', compact('fname', 'lname'));

    }
    
}
