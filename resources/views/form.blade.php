<!DOCTYPE html>
<html>
<head>
	<title>Form Sign Up</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>

	<h3>Sign Up Form</h3>
	
	<form action="welcome" method="post"><br>
	@csrf
	<label for="namadepan">First name:</label><br>
	<input id="namadepan" type="text" name="firstname"><br>
	<br>
	<label for="namabelakang">Last name:</label><br>
	<input id="namabelakang" type="text" name="lastname">

	<p>Gender</p>
	<input type="radio" name="gender">
	<label>Male</label> <br>
	<input type="radio" name="gender">
	<label>Female</label> <br>

	<p>Nationality:</p>
	<select name="nasionality">
		<option value="id">Indonesian</option>
		<option value="sg">Singaporean</option>
		<option value="mly">Malaysian</option>
		<option value="othr">Other</option>
	</select>

	<p>Language Spoken</p>
	<input type="checkbox" name="indonesia">Bahasa Indonesia <br>
	<input type="checkbox" name="english">English <br>
	<input type="checkbox" name="other">Other <br>

	<p>Bio:</p>
	<textarea rows="10" cols="30" >	</textarea>
	<br>
	<input type="submit" value="Sign Up">
	</form>

</body>
</html>